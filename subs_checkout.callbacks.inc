<?php


/**
 * Implements hook_subs_payment_payment_finish_alter()
 */
function subs_checkout_subs_payment_payment_finish_alter(Payment $payment) {
  $temp_subs = array();

  //Retrieve temporary subs created previously
  $temp_subs_ids = (!empty($payment->context_data['subs_ids'])) ? $payment->context_data['subs_ids'] : array();
  if ($temp_subs_ids) {
    $temp_subs = subs_load_multiple($temp_subs_ids);
  }

  //Temporary subs retrieve and payment succeed
  if ($temp_subs && payment_status_is_or_has_ancestor($payment->getStatus()->status, PAYMENT_STATUS_SUCCESS)) {

    $subs_types = array();
    $subs_updated = array();

    //Try to update subs status to their defaults status inherit from subs types
    try {

      //Retrieve subs types selected previously
      //Note that we also could retrieve them from session
      //using function subs_checkout_get_subs_type_selected()
      $subs_types_ids = (!empty($payment->context_data['subs_type_ids'])) ? $payment->context_data['subs_type_ids'] : array();
      if ($subs_types_ids) {
        $subs_types = entity_load('subs_type', $subs_types_ids);
        if (!$subs_types) {
          throw new Exception(t('Unable to retrieve previous subs types selected.'));
        }
      }

      //Try to update temporary subs
      $subs_updated = subs_checkout_update_temporary_subs_status($temp_subs, $subs_types);
    }
    //Error occured while trying to update status ?
    catch (Exception $e) {

      //Log exception message
      $msg = $e->getMessage();
      watchdog('subs_checkout', $msg, NULL, WATCHDOG_ERROR);

      //Log errors
      global $user;
      $vars = array(
        '@pid' => $payment->pid,
        '@uid' => $user->uid,
        '@subs_type_ids' => ($subs_types) ? implode(' - ', array_keys($subs_types)) : 'No subs types retrieve',
        '@subs_ids' => ($temp_subs) ? implode(' - ', array_keys($temp_subs)) : 'No temporary subs retrieve',
      );
      watchdog('subs_checkout', 'Payment pid : @pid succeed for user uid : @uid but system was unable to set default status to subs ids : @subs_ids for subs types : @subs_type_ids', $vars, WATCHDOG_ERROR);

      //Feedback to end-user
      $msg = variable_get('subs_checkout_message_subs_pending', FALSE);
      if (empty($msg['value'])) {
        $msg['value'] = t('An error occured while trying to update subscriptions statuses. There are in pending status for the moment. An administrator have been inform about it.');
      }
      drupal_set_message($msg['value'], 'warning');
    }

    //Use session rather than GET args
    //BTW : note that we don't have to use path arguments
    //as we could have further subs ids created an optionally payment id
    $_SESSION['subs_checkout']['pid'] = $payment->pid;
    $_SESSION['subs_checkout']['sids'] = (!empty($subs_updated)) ? array_keys($subs_updated) : array_keys($temp_subs);

    //Anyway, redirect to a default confirm page
    //as payment has succeed despite update subs could fail
    drupal_goto(SUBS_CHECKOUT_COMPLETE_PATH);
  }

  //Payment is'nt succeed : error occured or cancelled ?
  //Anyway, needs to cancel temporary subs previously created if any
  if (!$temp_subs) {
    watchdog('subs_checkout', 'Unable to load temporary subs previously created with ID\'s : @subs_ids.', array('@subs_ids' => $temp_subs_ids));
  }
  //Try to cancel temporary subs
  else {

    $subs_failed_cancel = subs_checkout_cancel_temporary_subs($temp_subs);
    //Fail to cancel all temporary subs ?
    if ($subs_failed_cancel) {

      global $user;
      $vars = array(
        '@pid' => $payment->pid,
        '@uid' => $user->uid,
        '@subs_ids' => implode(' - ', $subs_failed_cancel),
      );
      watchdog('subs_checkout', 'Error occured with payment pid : @pid and user uid : @uid. Failed to cancel subs ids : @subs_ids. These subs should be deleted.', $vars , WATCHDOG_ERROR);
    }
  }

  //Deals with cancelled payment in an another way than other failed payment
  if (payment_status_is_or_has_ancestor($payment->getStatus()->status, PAYMENT_STATUS_CANCELLED)) {

    $redirect_msg = variable_get('subs_checkout_cancel_url_message', FALSE);
    if (empty($redirect_msg['value'])) {
      $redirect_msg['value'] = t('Payment and subscriptions have been cancelled.');
    }
    drupal_set_message($redirect_msg['value'], 'warning');

    $redirect = variable_get('subs_checkout_cancel_url_redirect', '<front>');
    drupal_goto($redirect);
  }

  //Otherwise, display messages to only pending and failed parent status.
  if (payment_status_is_or_has_ancestor($payment->getStatus()->status, PAYMENT_STATUS_PENDING)) {
    $pending_message = variable_get('subs_checkout_payment_pending', FALSE);
    if (!empty($pending_message['value'])) {
      drupal_set_message($pending_message['value'], 'warning');
    }
  }
  elseif (payment_status_is_or_has_ancestor($payment->getStatus()->status, PAYMENT_STATUS_FAILED)) {
    $error_message = variable_get('subs_checkout_message_fail', FALSE);
    if (!empty($error_message['value'])) {
      drupal_set_message($error_message['value'], 'error');
    }
  }

  //Then redirect user to a "fail page"
  $redirect_path = variable_get('subs_checkout_redirect_path_fail', SUBS_CHECKOUT_PAYMENT_PATH);
  if (!$redirect_path) {
    $redirect_path = SUBS_CHECKOUT_PAYMENT_PATH;
  }

  //If redirect path doesn't redirect on payment checkout page, clean session
  if (substr($redirect_path, 0, strlen(SUBS_CHECKOUT_PAYMENT_PATH)) !== SUBS_CHECKOUT_PAYMENT_PATH) {
    //Clear session
    unset($_SESSION['subs_checkout']);
  }

  drupal_goto($redirect_path);
}