Subs checkout
---------

This is a contrib module to the subs module (http://drupal.org/project/subs).

It aims at showing a checkout form subs type selection
to the end user.
After submitting the checkout form, it will create a subs for
the current user based on subs type property.

By default, there is two hardcoded page process :
- cart : subs type selection
- complete : confirmation page

Only subs type non-subscribe by the current user will be show.

If default status from subs type is set to active, start date
will be set automatically (current time) and end date based
on start date + length of the subscription.

Otherwise, start date and end date will be set to 0
and this is in charge of an administrator to update subs.

In addition, it also support subs_payment_payment module
(http://drupal.org/project/subs_payment_payment).

In that case, it will add an another page to the checkout process :
- cart : selecting subs type
- payment : a quick summary (themable or change with settings)
  and a payment form
- complete page

Note that anonymous subscription are not allowed and by the way,
do not allow access to anonymous user.

A block should be used to display the checkout cart page.

-----------
REQUIREMENT
-----------

- subs (http://drupal.org/project/subs)

-------
INSTALL
-------

- Enable the module like any other contrib module
- Go to admin/config/workflow/subs/subs_checkout and define
 some settings
-> multiple subs type selection
-> details to display about subs type available (settings or themable)
-> complete page message
-> subs build mode to use on the complete page
If subs_payment_payment enable :
-> payment build mode to use on the complete page if any available

- Go to admin/people/permissions and define permissions
  for access to form page and settings

- Then, form should be accessible at : subscriptions/cart
  if at least one subs type exists and not already subscribe by the current user.

----
NOTE
----

PAYMENT REQUIRED CASE

By default, before trying to making a remote payment,
it will try to create temporary subs selected.
Subs status are set to PENDING.

If temporary subs creation succeed, then remote payment is executed. Otherwise,
it will rollback subs from SGBD.

If payment fail, it will try to set subs to CANCEL status. Otherwise (error to
cancel them), they are still in PENDING status.
That why if default status inherit from the subs types associated is set to
"PENDING" could make some confusion :
 - whatever it succeed or not, subs status will be the same as the inherited
   default subs type status defined.

Waiting from a workaround, this is strongly advice to don't set "PENDING"
default status to subs types.
It should be great if subs provide a hook to define own custom status. But it
needs a lot of rewrite code. To have a quick and dirty workaround, default
"PENDING" status is removed from subs types form and only subs types which
haven't a default status pending are shown on the checkout page.

If payment succeed, it will try to set the default status inherit from the subs
type used. If error occured at this state, end-user is inform about it and
administrator too. It means that subs created are still in PENDING status.

So you'll have to take a look into log reports regulary.

Subs with a CANCELLED status could be deleted. They exists only to keep track
on process failure. They must don't be set to any other status.
Indeed, a user could try again to subscribe to this subs types
and will create an another subs based on this subs type.

At the end of the process, subs could have "PENDING" status if :

1 - default inherit status from subs types (which shouldn't be allow)

2 - non-offsite payment method :
    - if payment failed : fail to cancel temporary subs
    - if payment succeed : fail to set inherit default status from subs type

In these cases, you could set appropriate status manually.

3 - offsite payment method :
    - if payment failed : fail to cancel temporary subs
    - if payment succeed : fail to set inherit default status from subs type
    - if payment pending : user haven't cancel or complete or still processing
      remote payment. He also doesn't go again on subs form selection.
      In that case, if changed subs date + remote session lifetime is greater
      than current time, you could cancel/delete subs and payment manually.
      But note that this job is already done :
        - if user try to hit the default callback form by going to
          'subscriptions/cart', this job will be done (based on action set into
          subs payment payment cron job settings).
        - by a cron job (hook from sdubs payment payment) which retrieve expired
          payment with the lifetime defined and associated subs.
