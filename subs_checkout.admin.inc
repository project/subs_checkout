<?php

function subs_checkout_settings ($form, &$form_state) {
  $form = array();

  $form['subs_checkout_multiple_subscriptions'] = array(
    '#type' => 'checkbox',
    '#title' => t('End user could select multiple subs type'),
    '#description' => t('End user could used checkboxes to choose further subs type. To use payment, need to define a currency code to override. Otherwise, a select list will be displayed and currency code of subs type will be used.'),
    '#default_value' => variable_get('subs_checkout_multiple_subscriptions', 1),
    '#options' => array(
      0 => t('Single'),
      1 => t('Multiple'),
    ),
  );

  $form['complete_page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Complete page settings'),
    '#description' => t('When a subscription succeed, the current user will be redirect to this page'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $msg_complete = variable_get('subs_checkout_complete_message', FALSE);
  $form['complete_page']['subs_checkout_complete_message'] = array(
    '#type' => 'text_format',
    '#title' => t('Complete page message'),
    '#description' => t('Enter a default text to display to end user when subscription succeed'),
    '#default_value' => (!empty($msg_complete['value'])) ? $msg_complete['value'] : '',
    '#format' => NULL,
    '#base_type' => 'textarea',
  );

  //If other view mode than defaut exists
  $entity_info = entity_get_info('subs');
  if (!empty($entity_info['view modes'])) {

    $options = array();
    foreach ($entity_info['view modes'] as $name => $property) {
      $options[$name] = $property['label'];
    }

    $form['complete_page']['subs_checkout_complete_subs_view_mode'] = array(
      '#type' => 'select',
      '#title' => t('Complete page view mode'),
      '#description' => t('Define which view mode subs entities should be used on the complete page'),
      '#options' => $options,
      '#default_value' => variable_get('subs_checkout_complete_view_mode', 0),
    ); 
  }

  if (module_exists('subs_payment_payment')) {
    //If other view mode than defaut exists
    $entity_info = entity_get_info('payment');
    if (!empty($entity_info['view modes'])) {

      $options = array();
      foreach ($entity_info['view modes'] as $name => $property) {
        $options[$name] = $property['label'];
      }

      $form['complete_page']['subs_checkout_complete_payment_view_mode'] = array(
        '#type' => 'select',
        '#title' => t('Complete page view mode'),
        '#description' => t('Define which view mode payment entity should be used on complete page'),
        '#options' => $options,
        '#default_value' => variable_get('subs_checkout_complete_payment_view_mode', 0),
      ); 
    }

    //Offsite payment method
    $form['payment'] = array(
      '#type' => 'fieldset',
      '#title' => t('Payment behavior'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $msg_pending_payment = variable_get('subs_checkout_payment_pending', FALSE);
    $form['payment']['subs_checkout_payment_pending'] = array(
      '#type' => 'text_format',
      '#title' => t('Message to display in case of pending payment'),
      '#default_value' => (!empty($msg_pending_payment['value'])) ? $msg_pending_payment['value'] : '',
      '#format' => NULL,
      '#base_type' => 'textarea',
    );

    $form['payment']['subs_checkout_cancel_url_redirect'] = array(
      '#type' => 'textfield',
      '#title' => t('Define cancel url to redirect the user.'),
      '#default_value' => variable_get('subs_checkout_cancel_url_redirect', '<front>'),
      '#description' => t('Do not include base path in the url.'),
    );

    $cancel_msg = variable_get('subs_checkout_cancel_url_message', FALSE);
    $form['payment']['subs_checkout_cancel_url_message'] = array(
      '#type' => 'text_format',
      '#title' => t('Cancel url message'),
      '#description' => t('Define which message to display in this case.'),
      '#default_value' => (!empty($cancel_msg['value'])) ? $cancel_msg['value'] : '',
      '#format' => NULL,
      '#base_type' => 'textarea',
    );
  }

  //Error behavior
  $form['error'] = array(
    '#type' => 'fieldset',
    '#title' => t('Error behavior'),
    '#description' => t('Define messages and redirect in case of error occured'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $msg_pending_subs = variable_get('subs_checkout_message_subs_pending', FALSE);
  $form['error']['subs_checkout_message_subs_pending'] = array(
    '#type' => 'text_format',
    '#title' => t('Message to display in case of pending subs status'),
    '#description' => t('This behaviour could occured when system cannot set subs status to its default subs type status. In that case, payment succeed but needs to prevent end-user. A default message his display otherwise.'),
    '#default_value' => (!empty($msg_pending_subs['value'])) ? $msg_pending_subs['value'] : '',
    '#format' => NULL,
    '#base_type' => 'textarea',
  );

  $msg_fail = variable_get('subs_checkout_message_fail', FALSE);
  $form['error']['subs_checkout_message_fail'] = array(
    '#type' => 'text_format',
    '#title' => t('Message to display in case of error occured. Note that if payment is enable, it could include cancelled payment.'),
    '#default_value' => (!empty($msg_fail['value'])) ? $msg_fail['value'] : '',
    '#format' => NULL,
    '#base_type' => 'textarea',
  );
  $form['error']['subs_checkout_redirect_path_fail'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to redirect in case of failure payment'),
    '#default_value' => variable_get('subs_checkout_redirect_path_fail', ''),
    '#description' => t('Do not include base path in the url. If not provided, form
      will be rebuild.'),
  );

  //Details parts
  $subs_types = entity_load('subs_type');
  if ($subs_types) {

    $form['subs_checkout_details'] = array(
      '#type' => 'fieldset',
      '#title' => t('Default subs type details'),
      '#description' => t('If no details have been defined for a subs type, it will be generated automatically based on fields and properties of the entity.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    foreach ($subs_types as $sub_type) {

      $subs_details = variable_get('subs_checkout_details_' . $sub_type->id, FALSE);
      $form['subs_checkout_details']['subs_checkout_details_' . $sub_type->id] = array(
        '#type' => 'text_format',
        '#title' => t('Enter default description for subs type @subs_type_name', array('@subs_type_name' => $sub_type->name)),
        '#default_value' => (!empty($subs_details['value'])) ? $subs_details['value'] : '',
        '#format' => NULL,
        '#base_type' => 'textarea',
      );
    }
  }

  return system_settings_form($form);
}
