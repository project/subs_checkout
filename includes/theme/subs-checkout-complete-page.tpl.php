<?php
/**
 * Available variable :
 *   - complete_message : a message to display to end user
 *   - subs_views_label : label to expose for sub
 *   - subs_views : an array containing renderable subs entites subscribe if any
 *   - payment_view_label : Label to expose for payment
 *   - payment_view : renderable payment entity if any
 */
?>

<div id="subs-checkout-complete-page-wrapper">

  <div class="subs-checkout-complete-messages">
    <?php if (isset($complete_message)) : ?>
    <?php print $complete_message; ?>
    <?php endif; ?>
  </div>

  <?php if (isset($subs_views)) : ?>
    <div class="subs-checkout-complete-subs-wrapper">

      <?php if (isset($subs_views_label)) : ?>
        <div class="subs-checkout-complete-subs-label">
          <?php print $subs_views_label; ?>
        </div>
      <?php endif; ?>

      <?php foreach($subs_views as $sid => $view) : ?>
        <div class="subs-checkout-complete-subs subs-checkout-complete-subs-<?php print $sid; ?>">
          <?php print $view; ?>
        </div>
      <?php endforeach; ?>

    </div>
  <?php endif; ?>

  <?php if (isset($payment_view)) : ?>

    <?php if (isset($payment_view_label)) : ?>
      <div class="subs-checkout-complete-payment-label">
        <?php print $payment_view_label; ?>
      </div>
    <?php endif; ?>

    <div class="subs-checkout-complete-payment">
      <?php print $payment_view; ?>
    </div>
  <?php endif; ?>

</div>