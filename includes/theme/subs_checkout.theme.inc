<?php

/**
 * Implements hook_preprocess_HOOK() 
 */
function subs_checkout_preprocess_subs_checkout_default_details(&$variables) {

  //Default field to display
  $fields = array(
    'name',
    'label',
    'length',
    'description',
    'grace',
    'currency_code',
    'amount',
    'quantity',
    'tax_rate',
  );

  $subs_type = $variables['subs_type'];
  if (get_class($subs_type) === 'SubsType' && $subs_type) {

    //Just iterate over each subs type attributes to display them...
    foreach ($subs_type as $attribute => $value) {

      if (in_array($attribute, $fields, TRUE)) {

        if ($value) {
          $variables[$attribute] = '';

          $variables['raw_' . $attribute] = $value;
          if ($attribute !== 'label') {
            $variables[$attribute] = ucfirst(str_replace('_', ' ', $attribute)) . ' : ';
          }

          if ($attribute === 'length') {
            module_load_include('inc', 'subs', 'subs.admin');
            $variables[$attribute] .= _subs_admin_secs_to_days($value) . ' ' . t('days');
          }
          else {
            $variables[$attribute] .= $value;
          }
        }
      }
    }

    //Deals with amount if property exists (module subs_payment enable)
    if (isset($variables['raw_amount']) && !empty($variables['raw_quantity']) && !empty($variables['raw_currency_code'])) {

      $amount = $variables['raw_amount'];
      $quantity = $variables['raw_quantity'];
      $tax_rate = (isset($variables['raw_tax_rate']) && ((float)$variables['raw_tax_rate'] > 0)) ? $variables['raw_tax_rate'] + 1 : 1;

      $total_amount = $amount * $quantity * $tax_rate;

      $variables['raw_price'] = $total_amount;
      $variables['price'] = t('Price') . ' : ' . payment_amount_human_readable($total_amount, $variables['raw_currency_code']);
    }
  }
}

/**
 * Implements hook_preprocess_HOOK() 
 */
function subs_checkout_preprocess_subs_checkout_complete_page(&$variables) {
  $sids = $variables['sids'];
  $pid = $variables['pid'];

  //Subs output
  $entity_info = entity_get_info('subs');
  $subs_view_mode = (isset($variables['subs_view_mode'])) ? $variables['subs_view_mode'] : '';
  $subs_view_mode = (!empty($entity_info['view modes']) && array_key_exists($subs_view_mode, $entity_info['view modes'])) ? $subs_view_mode : 'default';

  if ($sids) {
    
    $subscriptions = entity_load('subs', $sids);
    if ($subscriptions) {

      $views = entity_view('subs', $subscriptions, $subs_view_mode);
      if ($views) {

        $subs_views = array();
        foreach ($views as $sid => $view) {
          $subs_views[] = drupal_render($view);
        }

        if ($subs_views) {
          $variables['subs_views_label'] = t('Subscriptions') . ' : ';
          $variables['subs_views'] = $subs_views; 
        }
      }
    }
  }

  //Payment output
  if (is_numeric($pid)) {
    $payments = entity_load('payment', array($pid));
    $payment = array_shift($payments);
    if (payment_access('view', $payment)) {

      $entity_info = entity_get_info('payment');
      $payment_view_mode = (isset($variables['payment_view_mode'])) ? $variables['payment_view_mode'] : '';
      $payment_view_mode = (!empty($entity_info['view modes']) && array_key_exists($payment_view_mode, $entity_info['view modes'])) ? $payment_view_mode : 'default';

      $payment_view = entity_view('payment', array($payment), $payment_view_mode);
      $payment_rendered = drupal_render($payment_view);

      if ($payment_rendered) {
        $variables['payment_view_label'] = t('Payment order') . ' : ';
        $variables['payment_view'] = $payment_rendered;
      }
    }
  }
}
