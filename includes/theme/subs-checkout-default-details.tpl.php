<?php
/**
 * Available variable :
 * - each attributes attach to the subs type entity prefixed with 'raw_' keyword
 * - each attributes of subs type, containing attribute as label
 */
?>

<div class="subs-checkout-details subs-checkout-details-<?php if (isset($raw_id)) : print $raw_id; endif;?>">

  <div class="subs-checkout-details-label">
    <?php if (isset($label)) : ?>
    <?php print $raw_label; ?>
    <?php endif; ?>
  </div>

  <div class="subs-checkout-details-description">
    <?php if (isset($description)) : ?>
    <?php print $description; ?>
    <?php endif; ?>
  </div>

  <div class="subs-checkout-details-length">
    <?php if (isset($length)) : ?>
    <?php print $length; ?>
    <?php endif; ?>
  </div>

  <div class="subs-checkout-details-price">
    <?php if (isset($price)) : ?>
    <?php print $price; ?>
    <?php endif; ?>
  </div>

</div>