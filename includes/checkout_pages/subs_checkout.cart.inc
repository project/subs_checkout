<?php

/**
 * Checkout cart page
 */
function subs_checkout_page_cart($form, &$form_state) {
  global $user;

  //Clear session to rebuild checkout process if needed
  unset($_SESSION['subs_checkout']);

  $payment_enable = module_exists('subs_payment_payment');

  //If subs payment payment is enable, we could have some pending payment
  //with temporary subs which needs to be cleared before loading subs types per account
  if ($payment_enable) {
    subs_payment_payment_clean_pending_payment($user->uid, FALSE, 10);
  }

  //Retrieve available subs for the current account, including paying and free subs type
  $subs_types = subs_checkout_get_subs_types_availables_by_account($user, array(SUBS_STATUS_CANCELLED));

  //No subscriptions availables
  if (!$subs_types) {
    $all_subs_types = entity_load('subs_type');
    if ($all_subs_types && count($all_subs_types) > 0) {
      $form['no_new_subscriptions'] = array(
        '#type' => 'markup',
        '#markup' => t('You are currently subscribe to all subscriptions types availables.'),
        '#attributes' => array('class' => array('subs-cart-no-new-subs')),
      );
    }
    else {
      $form['no_subscriptions'] = array(
        '#type' => 'markup',
        '#markup' => t('There is currently no subscription type available.'),
        '#attributes' => array('class' => array('subs-cart-no-subs')),
      );
    }
    return $form;
  }
  //Store subscriptions for later
  $form['#subs_types'] = $subs_types;

  $subs_types_paying = $subs_types_free = array();

  //If payment exists, retrieve paying subs types from availables if any
  if ($payment_enable) {
    if (subs_payment_payment_methods_available()) {
      //@TODO for the moment, PENDING default status inherit are exclude from
      //paying subs types in order to prevent from conflict with workflow payment.
      //Should be a good option ro implements custom subs status
      $subs_types_paying = subs_checkout_get_paying_subs_types($subs_types, array(SUBS_STATUS_PENDING));
      $subs_types_free = array_diff_key($subs_types, $subs_types_paying);
    }
    else {
      watchdog('subs_checkout', 'There is no payment method enable. You must create one and enable it.', array(), WATCHDOG_WARNING);
    }
  }

  //Display subscriptions cart selection, including paying and free subs type

  if ($subs_types_paying) {
    $form['subs_cart_paying'] = array(
      '#type' => 'fieldset',
      '#title' => format_plural(count($subs_types_paying), 'Paying subscription', 'Paying subscriptions'),
      '#collapsible' => FALSE,
      '#collapsed' => TRUE,
      '#weight' => 0,
    );
  }

  if ($subs_types_free) {
    $form['subs_cart_free'] = array(
      '#type' => 'fieldset',
      '#title' => format_plural(count($subs_types_free), 'Free subscription', 'Free subscriptions'),
      '#collapsible' => FALSE,
      '#collapsed' => TRUE,
      '#weight' => 20,
    );
  }

  //Build subs type options
  $subs_paying_options = $subs_free_options = array();
  foreach ($subs_types as $subs_type) {
    $option = $subs_type->label;

    $details = variable_get('subs_checkout_details_' . $subs_type->id, FALSE);
    if (empty($details['value'])) {
      $details['value'] = theme('subs_checkout_default_details', array('subs_type' => $subs_type));
    }

    if ($details['value']) {
      $option .= '<div class="subs-cart-details">';
      $option .= $details['value'];
      $option .= '</div>';
    }

    //Store option into appropriate element
    if (array_key_exists($subs_type->id, $subs_types_paying)) {
      $subs_paying_options[$subs_type->id] = $option;
    }
    elseif (array_key_exists($subs_type->id, $subs_types_free)) {
      $subs_free_options[$subs_type->id] = $option;
    }
  }

  //Set options into appropriate wrapper
  $method_select = (variable_get('subs_checkout_multiple_subscriptions', 0)) ? 'checkboxes' : 'radios';
  if ($subs_paying_options) {
    $form['subs_cart_paying']['subs_type_paying'] = array(
      '#type' => $method_select,
      '#title' => t('Select a subscription'),
      '#options' => $subs_paying_options,
      '#attributes' => array('class' => array('subs-cart-subs-paying-select')),
    );
  }

  if ($subs_free_options) {
    $form['subs_cart_free']['subs_type_free'] = array(
      '#type' => $method_select,
      '#title' => t('Select a subscription'),
      '#options' => $subs_free_options,
      '#attributes' => array('class' => array('subs-cart-subs-free-select')),
    );
  }

  if ($subs_paying_options && $subs_free_options) {
    $form['markup'] = array(
      '#type' => 'markup',
      '#markup' => t('Or'),
      '#weight' => 10,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 50,
  );

  return $form;
}

/**
 * Function validate handler
 *
 * @see subs_checkout_page_cart()
 */
function subs_checkout_page_cart_validate(&$form, &$form_state) {
  $subs_types = array();

  //Retrieve subs type selected for each fields
  $subs_types_elements = array('subs_type_paying', 'subs_type_free');
  foreach ($subs_types_elements as $field) {
    if (isset($form_state['values'][$field]) && !empty($form_state['values'][$field])) {

      if (variable_get('subs_checkout_multiple_subscriptions', 0)) {
        $subs_types_ids = subs_checkout_subs_get_type_selected($form_state['values'][$field]);
        if ($subs_types_ids) {
          $subs_types[$field] = array_intersect_key($form['#subs_types'], $subs_types_ids);
        }
      }
      else {
        $subs_types_id = $form_state['values'][$field];
        if ($subs_types_id) {
          $subs_types[$field] = $form['#subs_types'][$subs_types_id];
        }
      }
    }
  }

  //Must have at least one subs type selected
  if (empty($subs_types)) {
    form_set_error('', t('You must select at least one subs type.'));
  }
  //Subs types free and paying selected are incompatible in submit process
  elseif (!empty($subs_types['subs_type_paying']) && !empty($subs_types['subs_type_free'])) {
    form_set_error('', t('You cannot select both paying and free subs types at the same time. Please select only paying subs types or free subs types.'));
  }
  //Otherwise, store subs types selected and kind of subs type (paying or free)
  else {

    if (!empty($subs_types['subs_type_paying'])) {
      $form_state['subs_types_entities'] = $subs_types['subs_type_paying'];
      $form_state['subs_types_step'] = 'paying';
    }
    elseif (!empty($subs_types['subs_type_free'])) {
      $form_state['subs_types_entities'] = $subs_types['subs_type_free'];
      $form_state['subs_types_step'] = 'free';
    }
  }
}

/**
 * Function submit handler
 *
 * @see subs_checkout_page_cart()
 */
function subs_checkout_page_cart_submit(&$form, &$form_state) {

  global $user;

  //Get subs type selected
  if (!empty($form_state['subs_types_entities'])) {

    //Asking for payment, redirect on payment page
    if ($form_state['subs_types_step'] === 'paying') {

      $_SESSION['subs_checkout']['subs_type_ids'] = array_keys($form_state['subs_types_entities']);
      $form_state['redirect'] = array(SUBS_CHECKOUT_PAYMENT_PATH);
    }
    //Otherwise, simple registration case
    elseif ($form_state['subs_types_step'] === 'free') {

      $subs = array();

      //Start transaction
      $trx = db_transaction();
      try {

        //Try to create subs first
        $subs = subs_checkout_create_subs($form_state['subs_types_entities']);
        if ($subs) {
          //Subs creation succeed, redirect to a default complete page
          $_SESSION['subs_checkout']['sids'] = array_keys($subs);
          $form_state['redirect'] = array(SUBS_CHECKOUT_COMPLETE_PATH);
        }
        //Error occured but no exception have been thrown
        else {
          $vars = array(
            '@uid' => $user->uid,
            '@username' => $user->name,
            '@subs_types_ids' => implode(', ', array_keys($form_state['subs_types_entities'])),
          );
          $error = t('Error occured while trying to create subs instance for user <a href="/user/@uid">@username</a>. Subscription type ids selected : @subs_types_ids.', $vars);

          throw new Exception($error);
        }
      }
      catch(Exception $e) {

        //Revert subs entities created and payment
        $trx->rollback();

        //Retrieve error message from system if any
        watchdog('subs_checkout', $e->getMessage());

        $error_message = variable_get('subs_checkout_message_fail', FALSE);
        if (empty($error_message['value'])) {
          $error_message['value'] = t('System could\'nt complete our request. Please try again or contact us at @contact.', array('@contact' => variable_get('site_mail', ini_get('sendmail_from'))));
        }
        drupal_set_message($error_message['value'], 'error');

        //Redirect to an error page if needed
        $redirect_path = variable_get('subs_checkout_redirect_path_fail', SUBS_CHECKOUT_CART_PATH);

        $form_state['redirect'] = array($redirect_path);
      }
    }
  }
  //Are we in a weird behaviour ?
  else {

    //End-user feedback
    $error_message = variable_get('subs_checkout_message_fail', FALSE);
    if (empty($error_message['value'])) {
      $error_message['value'] = t('System cannot complete your checkout cart. Please try again or contact us at @contact', array('@contact' => variable_get('site_mail', ini_get('sendmail_from'))));
    }
    drupal_set_message($error_message['value'], 'error');

    //Admin log
    $vars = array(
      '@uid' => $user->uid,
      '@username' => $user->name,
    );
    watchdog('subs_checkout', 'Internal error occured : unable to retrieve subs type entities selected to create for user <a href="/user/@uid">@username</a>.', $vars);
  }
}

/**
 * Function helper to retrieve
 * selected subs type from checkboxes 
 */
function subs_checkout_subs_get_type_selected($values) {
  $selected = array();

  foreach ($values as $subs_id => $value) {
    if ($value) {
      $selected[$subs_id] = $subs_id;
    }
  }

  return $selected;
}