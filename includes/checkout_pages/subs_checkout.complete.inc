<?php

/**
 * Complete checkout page 
 */
function subs_checkout_page_complete() {
  $output = '';

  $sids = $_SESSION['subs_checkout']['sids'];
  $pid = (!empty($_SESSION['subs_checkout']['pid'])) ? $_SESSION['subs_checkout']['pid'] : NULL;

  $msg_complete = variable_get('subs_checkout_complete_message', FALSE);
  if (empty($msg_complete['value'])) {
    $msg_complete['value'] = NULL;
  }

  //Prepare args for theme function
  $args = array(
    'sids' => $sids,
    'pid' => $pid,
    'complete_message' => $msg_complete['value'],
    'subs_view_mode' => variable_get('subs_checkout_complete_subs_view_mode', ''),
    'payment_view_mode' => variable_get('subs_checkout_complete_payment_view_mode', ''),
  );

  $output .= theme('subs_checkout_complete_page', $args);

  return $output;
}
