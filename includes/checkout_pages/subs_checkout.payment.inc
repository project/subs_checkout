<?php

/**
 * Checkout payment page
 */
function subs_checkout_page_payment($form, &$form_state) {

  $subs_types = subs_checkout_get_subs_type_selected();
  if (!$subs_types) {
    return drupal_access_denied();
  }

  //Store subs type for post process
  $form['#subs_types'] = $subs_types;

  //Groups details subs type infos
  $form['subs_checkout_wrapper']['details'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscription informations'),
    '#collapsible' => TRUE,
  );

  //Iterate over each subscriptions type selected to display
  //details on it and prepare payment object if needed
  foreach ($subs_types as $subs_id => $subs_type) {

    //Use custom details, otherwise display default themable details 
    //based on fields/properties of the entity
    $details = variable_get('subs_checkout_details_' . $subs_type->id, FALSE);
    if (empty($details['value'])) {
      $details['value'] = theme('subs_checkout_default_details', array('subs_type' => $subs_type));
    }

    if ($details['value']) {
      $form['subs_checkout_wrapper']['details'][$subs_id] = array(
        '#type' => 'markup',
        '#title' => t('Details about subscription @subs_name', array('@subs_name' => $subs_type->label)),
        '#markup' => $details['value'],
        '#prefix' => '<div class="subs-checkout-details-wrapper subs-checkout-details-wrapper-' . $subs_id . '">',
        '#suffix' => '</div>',
      );
    }
  }

  //Add a payment wrapper
  $form['subs_checkout_wrapper']['payment'] = array(
    '#type' => 'fieldset',
    '#title' => t('Payment'),
  );

  //Load subs payment payment form and add it as a children element
  module_load_include('inc', 'subs_payment_payment', 'subs_payment_payment.form');

  //Retrieve payment form
  $parents = array('subs_checkout_wrapper', 'payment');
  $payment_form = subs_payment_payment_form(array(), $form_state, $subs_types, array(), $parents);

  //Wrap payment form into a child element
  $form['subs_checkout_wrapper']['payment'] += $payment_form;

  //Add validate payment handlers if any
  if (!empty($payment_form['#validate'])) {
    $form['#validate'] = (!empty($form['#validate'])) ? array_merge($payment_form['#validate'], $form['#validate']) : $payment_form['#validate'];
  }

  //Store subs payment submit to execute it into a main submit handler
  $form_state['subs_payment_payment']['submit'] = $payment_form['#submit'];

  return $form;
}

/**
 * Submit handler form
 *
 * @see subs_checkout_page_payment()
 */
function subs_checkout_page_payment_submit(&$form, &$form_state) {
  $temp_subs = array();
  $subs_types = $form['#subs_types'];
  $succeed = FALSE;

  //Start temporary subs transaction
  $trx = db_transaction();
  try {

    //Try to create temporary subs first
    $temp_subs = subs_checkout_create_subs($subs_types, TRUE);

    //Fail to create temporary subs
    if (!$temp_subs) {

      $vars = array(
        '@uid' => $user->uid,
        '@username' => $user->name,
        '@subs_types_ids' => implode(', ', array_keys($subs_types)),
      );
      $error = t('Error occured while trying to create subs instance for user <a href="/user/@uid">@username</a>. Subscription type ids selected : @subs_types_ids.', $vars);

      throw new Exception($error);
    }

    //Commit transaction
    unset($trx);
  }
  catch (Exception $e) {
    //Revert temporary subs entities created
    $trx->rollback();
    watchdog('subs_checkout', $e->getMessage());
  }

  //Create temporary subs successfully
  if ($temp_subs) {

    try {

      //Executes payment module submit handlers after trying to create temporary subs.
      //Indeed, even if not logical, we cannot "revert" (e.g rollback)
      //a remote payment transaction which was succeed if after subs creation failed.
      //The weakness of this method if that error could occured while trying to change
      //subs pending status to default status inherit from its subs type.
      //Anyway, if errors occured at this state, we could track them.

      //Execute payment submit handlers and do other operations
      //into hook_subs_payment_payment_finish_alter() callbacks.
      $payment_submits = $form_state['subs_payment_payment']['submit'];
      foreach ($payment_submits as $function) {
        $function($form, $form_state, $temp_subs);
      }

      //If subs payment payment submit handlers succeed,
      //its means that redirect path have been build to execute the payment
      if (empty($form_state['subs_payment_payment_succeed']) || $form_state['subs_payment_payment_succeed'] !== TRUE || empty($form_state['redirect']) || substr($form_state['redirect'], 0, strlen(SUBS_PAYMENT_PAYMENT_EXECUTE_CALLBACK)) !== SUBS_PAYMENT_PAYMENT_EXECUTE_CALLBACK) {
        throw new Exception(t('No execute callback redirect have been founded.'));
      }

      //No error occurs at this step,
      //we could let's form redirect to the execute payment callback
      $succeed = TRUE;
    }
    catch (Exception $e) {

      //Log exeception message
      $msg = $e->getMessage();
      if ($msg) {
        watchdog('subs_checkout', $msg, NULL, WATCHDOG_ERROR);
      }

      //Try to cancel temporary subs
      $subs_failed_cancel = subs_checkout_cancel_temporary_subs($temp_subs);
      if ($subs_failed_cancel) {

        global $user;
        $vars = array(
          '@pid' => !empty($form_state['payment']->pid) ? $form_state['payment']->pid : 'Unknown',
          '@uid' => $user->uid,
          '@subs_ids' => implode(' - ', $subs_failed_cancel),
        );
        //Log that system fail to cancel temporary subs
        watchdog('subs_checkout', 'Error payment occured with payment pid : @pid and user uid : @uid. Failed to cancel subs ids : @subs_ids. These subs should be deleted.', $vars , WATCHDOG_ERROR);
      }
    }
  }

  //Error occured :
  // - unable to create temporary subs
  // - unexpected error exception throw while trying to execute submit form handler
  // - no redirect execute callback set into form by subs payment payment form
  if (!$succeed) {

    //Display error message to end-user and redirect him if needed
    $error_message = variable_get('subs_checkout_message_fail', FALSE);
    if (empty($error_message['value'])) {
      $error_message['value'] = t('An internal error occured and your payment request could\'nt be executed.');
    }
    drupal_set_message($error_message['value'], 'error');

    //Redirect to an error page if needed
    $redirect_path = variable_get('subs_checkout_redirect_path_fail', FALSE);
    if ($redirect_path) {

      //If redirect path doesn't redirect on payment checkout page, clean session
      if (substr($redirect_path, 0, strlen(SUBS_CHECKOUT_PAYMENT_PATH)) !== SUBS_CHECKOUT_PAYMENT_PATH) {
        //Clear session
        unset($_SESSION['subs_checkout']);
      }

      //To redirect, form don't have to be rebuild
      $form_state['rebuild'] = FALSE;
      $form_state['redirect'] = $redirect_path;
    }
  }
}

/**
 * Returns subs type selected previously in cart pages
 */
function subs_checkout_get_subs_type_selected() {
  $subs_type_selected = array();
  if (!empty($_SESSION['subs_checkout']['subs_type_ids'])) {
    $subs_type_ids = $_SESSION['subs_checkout']['subs_type_ids'];
    $subs_type_selected = array_intersect_key(entity_load('subs_type'), array_flip($subs_type_ids));
  }
  return $subs_type_selected;
}