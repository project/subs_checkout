<?php

define('SUBS_CHECKOUT_CART_PATH', 'subscriptions/cart');
define('SUBS_CHECKOUT_PAYMENT_PATH', 'subscriptions/payment');
define('SUBS_CHECKOUT_COMPLETE_PATH', 'subscriptions/complete');

/**
 * Implements hook_permission()
 */
function subs_checkout_permission() {
  return array(
    'access subs checkout' => array(
      'title' => t('Access to subs checkout pages'),
    ),
    'administer subs checkout' => array(
      'title' => t('Administer Subs checkout'),
    ),
  );
}

/**
 * Implements hook_menu() 
 */
function subs_checkout_menu() {
  $items = array();

  $items[SUBS_CHECKOUT_CART_PATH] = array(
    'title' => 'Subscription cart',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('subs_checkout_page_cart'),
    'access callback' => array('subs_checkout_access'),
    'access arguments' => array('cart'),
    'file' => 'includes/checkout_pages/subs_checkout.cart.inc',
    'type' => MENU_CALLBACK,
  );

  $items[SUBS_CHECKOUT_PAYMENT_PATH] = array(
    'title' => 'Subscription payment',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('subs_checkout_page_payment'),
    'access callback' => array('subs_checkout_access'),
    'access arguments' => array('payment'),
    'file' => 'includes/checkout_pages/subs_checkout.payment.inc',
    'type' => MENU_CALLBACK,
  );

  $items[SUBS_CHECKOUT_COMPLETE_PATH] = array(
    'title' => 'Subscription complete',
    'page callback' => 'subs_checkout_page_complete',
    'access callback' => array('subs_checkout_access'),
    'access arguments' => array('complete'),
    'file' => 'includes/checkout_pages/subs_checkout.complete.inc',
    'type' => MENU_CALLBACK,
  );

  $items['admin/config/workflow/subs/subs_checkout'] = array(
    'title' => 'Subs checkout',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('subs_checkout_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer subs checkout'),
    'file' => 'subs_checkout.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Function access to subs checkout pages
 */
function subs_checkout_access($page = '') {
  $access = FALSE;

  global $user;
  if ($user->uid && user_access('access subs checkout')) {

    switch($page) {
      case 'cart' :
        $access = subs_checkout_access_cart();
        break;

      case 'payment' :
        $access = subs_checkout_access_payment();
        break;

      case 'complete' :
        $access = subs_checkout_access_complete();
        break;

    }
  }

  return $access;
}

/**
 * Check if user could access to a cart checkout page
 */
function subs_checkout_access_cart() {
  return TRUE;
}

/**
 * Check if user could access to the payment checkout page
 */
function subs_checkout_access_payment() {

  if (!empty($_SESSION['subs_checkout']['subs_type_ids'])) {

    //Get subs type selected previously in checkout pages
    $subs_type_ids = $_SESSION['subs_checkout']['subs_type_ids'];

    //Get all subs types
    $subs_types = entity_load('subs_type');

    //All subs type ids provided exists ?
    if (count(array_intersect($subs_type_ids, array_keys($subs_types))) === count($subs_type_ids)) {

      //Get subs types selected previously
      $subs_types_selected = entity_load('subs_type', $subs_type_ids);
      if ($subs_types_selected) {

        //Check if subs type selected are still paying
        $subs_types_selected_filtered = subs_checkout_get_paying_subs_types($subs_types_selected, array(SUBS_STATUS_PENDING));
        if (count($subs_types_selected) === count($subs_types_selected_filtered)) {

          //Check if user don't already have these subcriptions types (except cancelled subs)
          $can_subscribe = subs_checkout_user_can_subscribe($subs_types_selected, NULL, array(SUBS_STATUS_CANCELLED));
          if ($can_subscribe) {
            return TRUE;
          }
        }
      }
    }
  }

  //Invalid subs type selected
  return FALSE;
}

/**
 * Check if a user could access to the complete page
 */
function subs_checkout_access_complete() {

  if (empty($_SESSION['subs_checkout']['sids'])) {
    return FALSE;
  }

  //Retrieve data from session
  $sids = $_SESSION['subs_checkout']['sids'];
  $pid = (!empty($_SESSION['subs_checkout']['pid'])) ? $_SESSION['subs_checkout']['pid'] : NULL;

  //Check if subs exists
  global $user;
  $user_subs = subs_load_by_user($user);
  if (!$user_subs || count(array_intersect($sids, array_keys($user_subs))) !== count($sids)) {
    return FALSE;
  }

  //Otherwise, check if payment exists if any
  if ($pid) {
    $payment = entity_load('payment', array($pid));
    if (!$payment) {
      return FALSE;
    }
  }
  
  return TRUE;
}

/**
 * Implements hook_theme()
 */
function subs_checkout_theme($existing, $type, $theme, $path) {
  //Be sure to use module theme path
  $path = drupal_get_path('module', 'subs_checkout') . '/includes/theme';

  return array(
    'subs_checkout_default_details' => array(
      'variables' => array('subs_type' => NULL),
      'template' => 'subs-checkout-default-details',
      'path' => $path,
      'file' => 'subs_checkout.theme.inc',
    ),
    'subs_checkout_complete_page' => array(
      'variables' => array('sids' => NULL, 'pid' => NULL, 'complete_message' => '', 'subs_view_mode' => '', 'payment_view_mode' => ''),
      'template' => 'subs-checkout-complete-page',
      'path' => $path,
      'file' => 'subs_checkout.theme.inc',
    ),
  );
}

/**
 * Implements hook_block_info()
 */
function subs_checkout_block_info() {
  $blocks = array();

  $blocks['subs_checkout_cart'] = array(
    'info' => t('Subs checkout cart'),
    'cache' => DRUPAL_NO_CACHE,
    'visibility' => BLOCK_VISIBILITY_NOTLISTED,
    'pages' => implode("\n", array(SUBS_CHECKOUT_CART_PATH, SUBS_CHECKOUT_PAYMENT_PATH, SUBS_CHECKOUT_COMPLETE_PATH)),
  );

  return $blocks;
}

/**
 * Implements hook_block_view()
 */
function subs_checkout_block_view($delta = '') {
  $block = array();

  switch($delta) {

    case 'subs_checkout_cart' :

      module_load_include('inc', 'subs_checkout', 'includes/checkout_pages/subs_checkout.cart');

      $block['subject'] = '';
      $block['content'] = drupal_get_form('subs_checkout_page_cart');
      break;
  }

  return $block;
}

/**
 * Implements hook_form_FORM_ID_alter()
 *
 * This is a quick and dirty workaround to remove
 * default PENDING status from subs type to prevent from
 * payment workflow conflict.
 *
 * A better solution would be a hook by subs module to provide
 * our own custom status but need a lot of work as not initially
 * designed for that.
 */
function subs_checkout_form_subs_type_form_alter(&$form, &$form_state) {
  if (module_exists('subs_payment_payment')) {
    unset($form['default_status']['#options'][SUBS_STATUS_PENDING]);
    drupal_set_message(t('PENDING default status have been remove to prevent from worflow payment conflict.'), 'warning');
  }
}

/**
 * Implements hook_subs_payment_payment_cron_payment_pending_cancel()
 */
function subs_checkout_subs_payment_payment_cron_payment_pending_cancel($subscriptions) {
  subs_checkout_cancel_temporary_subs($subscriptions);
}

/**
 * Implements hook_subs_payment_payment_cron_payment_pending_delete()
 */
function subs_checkout_subs_payment_payment_cron_payment_pending_delete($subscriptions) {
  subs_delete_multiple(array_keys($subscriptions));
}

/**
 * Returns a boolean indicating
 * if a user isn't already subscribe to
 * at least one of subscriptions types provided.
 * IMPORTANT : CANCEL subs are ignored. It means
 * that we could try to request a subs type that
 * previously failed.
 *
 * NOTE : it doesn't care about ACL or workflow
 *
 * @param $subs_types
 *   An array of subs types keyed by subs type ids
 * @param $account (optional)
 *   A user account to looking for.
 *   Default to the current user.
 * @param $forced_status
 *   An array of subs status to ignore
 *
 * @return
 *   TRUE : user could subscribe to all subs types
 *   FALSE otherwise (at least one subs types is already used)
 */
function subs_checkout_user_can_subscribe(array $subs_types, $account = NULL, $forced_status = array()) {

  if (!$account) {
    global $user;
    $account = $user;
  }

  $subs_subcribe = subs_load_by_user($account);
  if ($subs_subcribe) {
    foreach($subs_subcribe as $subs) {

      if (!in_array((int)$subs->status, $forced_status, TRUE)) {
        foreach($subs_types as $subs_type) {
          if ($subs->type === $subs_type->name) {
            return FALSE;
          }
        }
      }
    }
  }

  return TRUE;
}

/**
 * Function helper to retrieve subs types
 * available (not already used) for an account given
 *
 * @param $account (optional)
 *   A user account object
 * @param $forced_status
 *   An array containing subs status to retrieve anyway.
 *
 * @return
 *   An array of subs type available.
 */
function subs_checkout_get_subs_types_availables_by_account($account = NULL, $forced_status = array()) {

  if (!$account) {
    global $user;
    $account = $user;
  }

  $subs_types = entity_load('subs_type');
  if ($subs_types) {

    $user_subscriptions = subs_load_by_user($account);
    if ($user_subscriptions) {

      foreach ($user_subscriptions as $subs) {

        //Exclude forced status
        if (!in_array((int)$subs->status, $forced_status, TRUE)) {

          foreach ($subs_types as $subs_type) {
            if ($subs->type === $subs_type->name) {
              unset($subs_types[$subs_type->id]);
              break;
            }
          }
        }
      }
    }
  }

  return $subs_types;
}

/**
 * Filter and returns only subs types provided
 * which are paying.
 *
 * NOTE : Should only be used if subs payment payment module is enable
 *
 * @param $subs_types
 *   An array keyed by subs type id and containing subs types
 *   to filter.
 * @param $excluded_default_status
 *   An array of subs types default status to exclude from
 *   paying subs types.
 *
 * @return
 *   The filtered subs types array passed.
 */
function subs_checkout_get_paying_subs_types($subs_types, $excluded_default_status = array()) {
  $subs_payment_payment = module_exists('subs_payment_payment');

  foreach ($subs_types as $key => $subs_type) {

    //Default status should be excluded ?
    if (in_array((int)$subs_type->default_status, $excluded_default_status, TRUE)) {
      unset($subs_types[$key]);
    }
    //Otherwise, subs type haven't all payment field properties ?
    elseif ($subs_payment_payment && !subs_payment_payment_subs_type_is_paying($subs_type)) {
      unset($subs_types[$key]);
    }
  }

  return $subs_types;
}

/**
 * Function helper to create
 * subscriptions based on
 * subs type selected except status
 * which is set to be temporary (PENDING)
 *
 * @param subs_types
 *   An array containing subs type entities
 * @param $temp
 *   A boolean indicating if subs created
 *   must be real (inherit from subs type) or temporary
 * @param $user (optional)
 *   An account user to create subs.
 *   If not provided, default to the current user
 *
 * @return
 *   An array containing subs created for the user 
 *   specified (or current) on success
 *   An empty array otherwise.
 */
function subs_checkout_create_subs($subs_types, $temp = FALSE, $user = NULL) {
  $subs = array();
  
  if (!$user) {
    global $user;
  }

  $trx = db_transaction();
  try {

    foreach ($subs_types as $subs_type) {

      //Set default pending properties to temporary subs
      $date_start = 0;
      $date_end = 0;
      $status = SUBS_STATUS_PENDING;

      //If not temporary, set inherit values
      if ($temp !== TRUE) {
        $date_start = ((int)$subs_type->default_status === SUBS_STATUS_ACTIVE) ? REQUEST_TIME : 0;
        $date_end = ($date_start > 0 && $subs_type->length > 0) ? $date_start + $subs_type->length : 0;
        $status = $subs_type->default_status;
      }

      //Construct subs entity
      $values = array(
        'type' => $subs_type->name,
        'uid' => $user->uid,
        'date_start' => $date_start,
        'date_end' => $date_end,
        'status' => $status,
      );
      $subscription = subs_create($values);

      //Save entity
      subs_save($subscription);

      $subs[$subscription->sid] = $subscription;
    }

    //Commit all subs created / or release savepoint
    unset($trx);
  }
  catch(Exception $e) {
    $trx->rollback();
    throw $e;
  }

  return $subs;
}

/**
 * Function helper to set inherit status
 * from subs types associated as "models".
 * Not that's an atomic operation : all subs
 * must be updated or nothing.
 *
 * @param $subscriptions
 *   An array of temporary subscriptions
 * @param $subs_types
 *   An array of subs types associated to
 *   temporary subscriptions provided.
 *   Defaults to all subs types.
 *
 * @throws Exception
 *   If one or further errors occured such as :
 *   - internal subs error
 *   - a temporary subs provided did'nt have its
 *     corresponding subs types associated.
 */
function subs_checkout_update_temporary_subs_status(array $subscriptions, array $subs_types = array()) {

  $trx = db_transaction();
  try {

    if (!$subs_types) {
      $subs_types = subs_get_types();
    }

    foreach ($subscriptions as $sid => $subs) {
      $i = 0;
      foreach ($subs_types as $id => $subs_type) {
        if ($subs->type === $subs_type->name) {

          //For the moment, only start date auto supported
          //if default subs type status is active
          $date_start = ((int)$subs_type->default_status === SUBS_STATUS_ACTIVE) ? REQUEST_TIME : 0;

          $subs->status = $subs_type->default_status;
          $subs->date_start = $date_start;
          $subs->date_end = ($date_start > 0 && $subs_type->length > 0) ? $date_start + $subs_type->length : 0;

          subs_save($subs);
          $subscriptions[$sid] = $subs;
          break;
        }
        $i++;
      }

      //Cannot found subs type associated to the subs iterated ?
      //Revert all subs created previously
      if ($i >= count($subs_types)) {
        throw new Exception (t('Subs checkout - Unable to retrieve subs types associated to the temporary subs with sid @sid', array('@sid' => $subs->sid)));
      }
    }
    unset($trx);
  }
  catch (Exception $e) {
    $trx->rollback();
    throw $e;
  }

  return $subscriptions;
}

/**
 * Function helper to set furthers
 * subs to cancel status
 *
 * @param $subscriptions
 *   An array of subs to set to cancel status
 *
 * @return
 *   An array of subs failed
 */
function subs_checkout_cancel_temporary_subs(array $subscriptions) {
  $subs_failed = array();

  foreach ($subscriptions as $subscription) {
    $return = subs_set_cancelled($subscription);
    if (!$return) {
      $subs_failed[$subscription->sid] = $subscription->sid;
    }
  }

  return $subs_failed;
}
